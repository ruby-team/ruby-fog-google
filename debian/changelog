ruby-fog-google (1.19.0-3) unstable; urgency=medium

  * Reupload to unstable
  * Drop obsolete X{S,B}-Ruby-Versions fields
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Jun 2023 12:38:38 +0530

ruby-fog-google (1.19.0-2) experimental; urgency=medium

  * Allow fog-core 2.1 to satisfy dependency requirement

 -- Pirate Praveen <praveen@debian.org>  Fri, 10 Mar 2023 00:57:45 +0530

ruby-fog-google (1.19.0-1) experimental; urgency=medium

  * New upstream version 1.19.0
  * Refresh patches
  * Update (build) dependencies

 -- Pirate Praveen <praveen@debian.org>  Tue, 07 Mar 2023 16:28:21 +0530

ruby-fog-google (1.15.0-3) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Set upstream metadata fields: Security-Contact.
  * Update standards version to 4.6.1, no changes needed.

  [ Antonio Terceiro ]
  * Add patch to relax dependency on fog-core

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 28 Sep 2022 18:42:55 -0300

ruby-fog-google (1.15.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sun, 21 Nov 2021 21:00:59 +0530

ruby-fog-google (1.15.0-1) experimental; urgency=medium

  * Revert "Switch to gitlab's fork as upstream source"
    (gitlab has switched back to original gem)
  * New upstream version 1.15.0

 -- Pirate Praveen <praveen@debian.org>  Mon, 20 Sep 2021 23:35:27 +0530

ruby-fog-google (1.14.0-1) unstable; urgency=medium

  * Reupload to unstable
  * Use newer watch file standard
  * New upstream version 1.14.0

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Aug 2021 17:06:43 +0530

ruby-fog-google (1.13.0-2) experimental; urgency=medium

  * Add ruby-addressable as new (build) dependency
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility level to 13

 -- Pirate Praveen <praveen@debian.org>  Wed, 25 Aug 2021 01:58:15 +0530

ruby-fog-google (1.13.0-1) experimental; urgency=medium

  * Switch to gitlab's fork as upstream source
  * New upstream version 1.13.0

 -- Pirate Praveen <praveen@debian.org>  Tue, 13 Apr 2021 14:14:41 +0530

ruby-fog-google (1.12.1-1) experimental; urgency=medium

  * New upstream version 1.12.1
  * Update minimum version of ruby-google-api-client to 0.44.2

 -- Pirate Praveen <praveen@debian.org>  Mon, 22 Feb 2021 21:40:10 +0530

ruby-fog-google (1.11.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * New upstream version 1.11.0
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Add ruby-google-cloud-env as dependency

 -- Pirate Praveen <praveen@debian.org>  Sat, 30 Jan 2021 16:46:49 +0530

ruby-fog-google (1.10.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 29 Jun 2020 23:00:00 +0530

ruby-fog-google (1.10.0-1) experimental; urgency=medium

  * New upstream version 1.10.0
  * Update minimum version of ruby-google-api-client to 0.32
  * Drop patches, not required anymore
  * Bump Standards-Version to 4.5.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sat, 20 Jun 2020 00:46:16 +0530

ruby-fog-google (1.9.1-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * New upstream version 1.9.1
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Bump Standards-Version to 4.4.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Wed, 04 Dec 2019 21:38:54 +0530

ruby-fog-google (1.8.2-2) unstable; urgency=medium

  * Relax dependency on googleauth

 -- Pirate Praveen <praveen@debian.org>  Tue, 05 Mar 2019 21:17:17 +0530

ruby-fog-google (1.8.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.8.2
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Disable patches as it were no longer applicable
  * Fix d/copyright
  * Add d/upstream/metadata

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 23 Jan 2019 22:59:27 +0530

ruby-fog-google (1.8.1-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Nov 2018 10:29:28 +0530

ruby-fog-google (1.8.1-1) experimental; urgency=medium

  * New upstream version 1.8.1
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Relax fog-json dependency in gemspec
  * Tighten dependencies

 -- Pirate Praveen <praveen@debian.org>  Tue, 20 Nov 2018 16:18:01 +0530

ruby-fog-google (1.3.3-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 04 Jun 2018 10:31:11 +0530

ruby-fog-google (1.3.3-1) experimental; urgency=medium

  * New upstream version 1.3.3
  * Bump Standards-Version to 4.1.4 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Refresh packaging with dh-make-ruby -w
  * Add ruby-google-api-client as dependency

 -- Pirate Praveen <praveen@debian.org>  Wed, 09 May 2018 18:22:02 +0530

ruby-fog-google (0.5.3-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sat, 19 Aug 2017 09:18:39 +0530

ruby-fog-google (0.3.2-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sun, 12 Jun 2016 22:15:13 +0530

ruby-fog-google (0.0.7-1) unstable; urgency=medium

  * Initial release (Closes: #792288)

 -- Pirate Praveen <praveen@debian.org>  Mon, 13 Jul 2015 20:55:11 +0530
